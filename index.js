const Base = require('codeceptjs/node_modules/mocha/lib/reporters/base');
const ms = require('codeceptjs/node_modules/mocha/lib/ms');
const fs = require('fs');
const event = require('codeceptjs/lib/event');
const AssertionFailedError = require('codeceptjs/lib/assert/error');
const output = require('codeceptjs/lib/output');
const TestCasesToolBuilder = require('test-cases-tool-builder-js');
const testCasesToolBuilder = new TestCasesToolBuilder();

const cursor = Base.cursor;
let currentMetaStep = null;
let feature = null;

class TestCasesToolCodeceptJS extends Base {
    constructor(runner, opts) {
        super(runner);
        let level = 1;
        opts = opts.reporterOptions || opts;
        if (opts.steps) level = 1;
        if (opts.debug) level = 2;
        if (opts.verbose) level = 3;

        output.level(level);
        output.print(`Using test root "${global.codecept_dir}"`);

        const showSteps = level >= 1;

        const indents = 0;
        function indent() {
            return Array(indents).join('  ');
        }

        function scenarioFormatter(scenario, status, err=null) {
            let scenarioModified = {
                directory: scenario.file
                    .replace(`${global.codecept_dir}/${opts.testCasesTool.baseDir}`, "")
                    .replace(/(.*).js/, "").split("/"),
                feature: feature,
                scenario: scenario.title,
                duration: ms(scenario.duration || 5),
                status: status,
                location: scenario.file
                    .replace(global.codecept_dir, ""),
                exception: err ? err.toString() : null
            };
            return scenarioModified;
        }

        function stepGrabber(scenario) {
            let pattern = `(${scenario.title})(.+)((\\s)+(.+))+(})`;
            let whole_steps = [];
            fs.readFileSync(scenario.file).toString().match(pattern).toString().split("\n").forEach(function (line) {
                if (line.match(/step\((.*),/)) {
                    whole_steps.push({
                        step: line.match(/step\((.*),/)[1].toString().split('"').join('').split("'").join(''),
                        status: null});
                }
            });
            return whole_steps;
        }

        runner.on('start', () => {
            console.log();
            testCasesToolBuilder.config(opts.testCasesTool);
            testCasesToolBuilder.startSuite();
        });

        runner.on('suite', (suite) => {
            feature = [suite.title];
            output.suite.started(suite);
        });

        runner.on('fail', (test, err) => {
            testCasesToolBuilder.buildScenario(scenarioFormatter(test, 'failed', err));
            if (showSteps && test.steps) {
                return output.scenario.failed(test);
            }
            cursor.CR();
            output.test.failed(test);
        });

        runner.on('pending', (test) => {
            testCasesToolBuilder.buildScenario(scenarioFormatter(test, 'pending'), stepGrabber(test));
            cursor.CR();
            output.test.skipped(test);
        });

        runner.on('pass', (test) => {
            testCasesToolBuilder.buildScenario(scenarioFormatter(test, 'passed'));
            if (showSteps && test.steps) {
                return output.scenario.passed(test);
            }
            cursor.CR();
            output.test.passed(test);
        });

        if (showSteps) {
            runner.on('test', (test) => {
                testCasesToolBuilder.startScenario();
                currentMetaStep = '';
                if (test.steps) {
                    output.test.started(test);
                }
            });

            event.dispatcher.on(event.step.started, (step) => {
                if (step.metaStep) {
                    if (currentMetaStep !== step.metaStep.toString()) {
                        output.step(step.metaStep);
                        currentMetaStep = step.metaStep.toString();
                    }
                } else {
                    currentMetaStep = '';
                }
                output.step(step);
            });
            event.dispatcher.on(event.step.passed, step => output.stepExecutionTime(step));
            event.dispatcher.on(event.step.failed, step => output.stepExecutionTime(step));
        }

        runner.on('end', this.result.bind(this));
    }

    result() {
        const stats = this.stats;
        console.log();
        let status = null;

        // passes
        if (stats.failures) {
            output.print('-- FAILURES:');
        }

        if (stats.failures) {
            status = 'failed'
        } else {
            status = 'passed'
        }

        // failures
        if (stats.failures) {
            // append step traces
            this.failures.forEach((test) => {
                const err = test.err;
                if (err instanceof AssertionFailedError) {
                    err.message = err.cliMessage();

                    // remove error message from stacktrace
                    const lines = err.stack.split('\n');
                    lines.splice(0, 1);
                    err.stack = lines.join('\n');
                }
                if (output.level() < 3) {
                    err.stack += '\n\nRun with --verbose flag to see NodeJS stacktrace';
                }
            });

            Base.list(this.failures);
            console.log();
        }
        testCasesToolBuilder.stopSuite(status, ms(stats.duration));
        output.result(stats.passes, stats.failures, stats.pending, ms(stats.duration));
    }
}

global.step = function (name, stepDefinition=null) {
    if (stepDefinition) {
        try {
            stepDefinition.call(this, arguments);
        }
        catch(error) {
            console.log(error);
        }
    }
    testCasesToolBuilder.step({step: name, status: null});
};

module.exports = function (runner, opts) {
    return new TestCasesToolCodeceptJS(runner, opts);
};